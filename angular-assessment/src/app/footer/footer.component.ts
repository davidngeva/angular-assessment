import { Component, OnInit } from '@angular/core';
import {DataCacheService} from '../services/data-cache.service';
import {MaxRowData} from '../models/clientData';
import {Router} from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  maxRowData: MaxRowData;
  pagination: any[];
  totalLength: number;
  currentPage = 0;
  constructor(
    private dataService: DataCacheService,
    public router: Router
  ) { }

  ngOnInit() {
    this.dataService.clientDataSourceObservable.subscribe(res => {
      this.totalLength = res.length;
      this.changePagination();
    });
    this.dataService.maxRowsObservable.subscribe((res) => {
      this.maxRowData = res;
      this.changePagination();
    });
  }

  changePagination(toPage?:  number) {
    if (this.maxRowData && this.maxRowData.rowsNumber && this.totalLength) {
      const maxPage = Math.ceil(this.totalLength / this.maxRowData.rowsNumber);
      const initRow = (toPage || 0) * this.maxRowData.rowsNumber;
      this.pagination = new Array(maxPage);
      if (typeof toPage === 'number') {
        this.dataService.changeMaxRows({
          rowsNumber: this.maxRowData.rowsNumber,
          startRow: initRow
        });
        this.currentPage = toPage;
      }
    }
    return false;
  }

}
