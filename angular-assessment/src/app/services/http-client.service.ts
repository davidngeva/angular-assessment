import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpEvent} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {map, tap, catchError} from 'rxjs/operators';

@Injectable()
export class HttpClientService {
    constructor(
        private httpClient: HttpClient
    ) {}

    public get (serviceURL: string): Observable<any> {
        return this.httpClient.get(serviceURL);
    }

    public post (serviceURL: string, body: any, options?: Object): Observable<any> {
        console.log('options', options);
        return  this.httpClient.post(serviceURL, body, options)
                .pipe(
                    map((response: HttpEvent<any>) => {
                        console.log('response', response);
                        return response;
                    }),
                    tap(response => response),
                    catchError(this.handleError(serviceURL))
                );
    }

    private handleError<T> (serviceURL = '') {
        return (error: HttpErrorResponse) => {
            console.log('serviceURL', serviceURL);
            return of(error.message);
        };
    }
}
