import {Injectable} from '@angular/core';
import {ClientData, MaxRowData} from '../models/clientData';
import {HttpClientService} from './http-client.service';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class DataCacheService {
    constructor(
        private httpService: HttpClientService
    ) {}

    public globalClientDataCache: ClientData[];
    private clientDataSource = new BehaviorSubject<ClientData[]>([]);
    public clientDataSourceObservable = this.clientDataSource.asObservable();

    private maxRows = new BehaviorSubject<MaxRowData>({
        rowsNumber: 50,
        startRow: 0
    });
    public maxRowsObservable = this.maxRows.asObservable();

    globalHeader: ClientData = {
        action: 'medium',
        name: 'medium',
        phone: 'medium',
        email: 'large',
        company: 'medium',
        date_entry: 'medium',
        org_num: 'medium',
        address_1: 'medium',
        city: 'medium',
        zip: 'medium',
        geo: 'medium',
        pan: 'medium',
        pin: 'medium',
        id: 'small',
        status: 'medium',
        fee: 'small',
        guid: 'large',
        date_exit: 'medium',
        date_first: 'medium',
        date_recent: 'medium',
        url: 'medium'
    };

    getClientData() {
        this.httpService.get('/assets/sample_data.json').subscribe(response => {
            this.clientDataSource.next(response);
            this.globalClientDataCache = response;
        });
    }
    changeMaxRows(row: MaxRowData) {
        this.maxRows.next(row);
    }
    // clientDataList: ClientData[] = [];
    // maxRows: number
}
