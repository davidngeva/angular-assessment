import { Component, OnInit } from '@angular/core';
import {HttpClientService} from './services/http-client.service';
import {DataCacheService} from './services/data-cache.service';
import {of} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor (
    private httpService: HttpClientService,
    private dataService: DataCacheService
  ) {}

  ngOnInit () {
    this.dataService.getClientData();
  }
}
