import { Component, OnInit } from '@angular/core';
import {DataCacheService} from '../services/data-cache.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  maxRowNumber: number;
  headers: string[];
  constructor(
    private dataService: DataCacheService,
    public router: Router
  ) {}

  ngOnInit() {
    this.dataService.maxRowsObservable.subscribe((res) => {
      this.maxRowNumber = res.rowsNumber;
    });
  }

  changeMaxRow(event) {
    this.dataService.changeMaxRows({
      rowsNumber: Number(event.target.value),
      startRow: 0
    });
  }

  getClassName(name: string) {
    return {'highlight': name === this.router.url.replace('/', '')};
  }
}
