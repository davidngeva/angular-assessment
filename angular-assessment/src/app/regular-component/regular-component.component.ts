import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {HttpClientService} from '../services/http-client.service';
import {DataCacheService} from '../services/data-cache.service';
import { ClientData, MaxRowData } from '../models/clientData';

@Component({
  selector: 'regular-component',
  templateUrl: './regular-component.component.html',
  styleUrls: ['./regular-component.component.css']
})
export class RegularComponent implements OnInit {

  dataList: ClientData[] = [];
  displayList: ClientData[] = [];
  maxRow: MaxRowData;
  headers: ClientData = {};
  @ViewChild('maintable') maintable: ElementRef;
  @ViewChild('mainHead') mainHead: ElementRef;
  constructor(
    private httpService: HttpClientService,
    private dataService: DataCacheService
  ) {}

  ngOnInit() {
    this.dataService.clientDataSourceObservable.subscribe((data) => {
      data.forEach((ele) => {ele.action = 'action'; });
      this.dataList = data;
      this.refreshRows();
    });
    this.dataService.maxRowsObservable.subscribe((row) => {
      this.maxRow = row;
      this.refreshRows();
    });
  }

  refreshRows() {
    if (this.dataList.length > 0 && this.maxRow) {
      // this.headers = [Object.keys(this.dataList[0]).reduce(function(o, val) { o[val] = val; return o; }, {})];
      this.headers = this.dataService.globalHeader;
      this.displayList = this.dataList.slice(this.maxRow.startRow, this.maxRow.startRow + this.maxRow.rowsNumber);
    }
  }

  sendRowData(data: ClientData) {
    this.httpService.post('/api/submit', {...data}, {'Content-Type': 'application/json', 'Accept': 'application/json'}).subscribe((res) => {
      console.log('response from backend', res);
    });
  }

  checkClassName(key) {
    return {[this.headers[key]]: true};
  }

}
