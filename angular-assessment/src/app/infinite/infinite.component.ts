import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, Renderer2 } from '@angular/core';
import {DataCacheService} from '../services/data-cache.service';
import {HttpClientService} from '../services/http-client.service';
import {ClientData} from '../models/clientData';
import {fromEvent, Subscription} from 'rxjs';

@Component({
  selector: 'infinite',
  templateUrl: './infinite.component.html',
  styleUrls: ['./infinite.component.css']
})
export class InfiniteComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('Infinite') Infinite: ElementRef;
  displayList: ClientData[] = [];
  headers = {};
  scrollListener: () => void;
  loading = false;
  constructor(
    private dataService: DataCacheService,
    private httpService: HttpClientService,
    private renderer2: Renderer2
  ) {
    this.infiniteLoad = this.infiniteLoad.bind(this);
  }

  ngOnInit() {
    this.dataService.clientDataSourceObservable.subscribe(res => {
      res.forEach((ele) => {ele.action = 'action'; });

      this.displayList = this.displayList.concat(res);
      res.length > 0 && this.enrichHeaders(res[0]);
      this.loading = false;
    });
    if (this.dataService.globalClientDataCache && this.dataService.globalClientDataCache.length > 0 || this.displayList.length > 0) {
      this.displayList = this.dataService.globalClientDataCache.slice();
      this.displayList.length > 0 && this.enrichHeaders(this.displayList[0]);
    } else {
      this.dataService.getClientData();
    }
  }

  ngAfterViewInit() {
    this.scrollListener = this.renderer2.listen(document.querySelector('.content'), 'scroll', this.infiniteLoad);
  }

  infiniteLoad(e) {
    e.stopPropagation();
    const tempNode = document.querySelector('.content');
      if ((tempNode.scrollTop + 10 + tempNode.clientHeight) >= tempNode.scrollHeight && !this.loading) {
        this.loading = true;
        this.dataService.getClientData();
      }
  }

  enrichHeaders(clientData: ClientData) {
    // for dynamic set the header according to data coming;
    const tempClientData = JSON.parse(JSON.stringify(clientData));
    Object.keys(tempClientData).map(function(key) { tempClientData[key] = 'medium'; }, {});
    this.headers = {
      ...tempClientData,
      ...this.dataService.globalHeader
    };
  }

  sendRowData(data: ClientData) {
    this.httpService.post('/api/submit', {...data}, {'Content-Type': 'application/json', 'Accept': 'application/json'}).subscribe((res) => {
      console.log('response from backend', res);
    });
  }

  checkClassName(key) {
    return {[this.headers[key]]: true};
  }

  ngOnDestroy() {
    this.scrollListener && this.scrollListener();
  }
}
