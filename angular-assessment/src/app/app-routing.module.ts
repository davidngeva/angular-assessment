import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegularComponent } from './regular-component/regular-component.component';
import { InfiniteComponent } from './infinite/infinite.component';

const routes: Routes = [
  { path: '', redirectTo: '/regular', pathMatch: 'full'},
  { path: 'regular', component: RegularComponent},
  { path: 'infinite', component: InfiniteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
