import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/Http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegularComponent } from './regular-component/regular-component.component';
import { InfiniteComponent } from './infinite/infinite.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import {HttpClientService} from './services/http-client.service';
import {DataCacheService} from './services/data-cache.service';

@NgModule({
  declarations: [
    AppComponent,
    RegularComponent,
    InfiniteComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    HttpClientService,
    DataCacheService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
